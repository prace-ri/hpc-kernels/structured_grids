# README - Cellular Automaton Example

## Description

In this example, we demonstrate how modern MPI-3 features can be used in structured grid codes for implementing **halo data exchange** (i.e. the exchange of *ghost cell data*) and for **parallel I/O** of structured grid data.

Two code versions of this example are available, one written in C and the other in C++14 (for individual descriptions, see the `README.md` files in the `wireworld_c` and `wireworld_c++` subdirectories).

### Cellular automata and Wireworld

Simulations of [cellular automata](https://en.wikipedia.org/wiki/Cellular_automaton), such as for example [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life), are particularly comprehensible and easy-to-implement instances of structured-grid codes. In this example however, we present an implementation of the less-known and Turing-complete cellular automaton [Wireworld](https://en.wikipedia.org/wiki/Wireworld), which is particularly suited for simulating electronic logic elements.

The cells in Wireworld have exactly one of the following four states:

 * *Electron head*
 * *Electron tail*
 * *Wire*
 * *Empty*

The state transitions are given by:

 * *Electron head* always becomes *Electron tail*
 * *Electron tail* always becomes *Wire*.
 * *Wire* becomes *Electron head*, if the number of *Electron heads* in the 8 neighboring cells is 1 or 2, otherwise it stays *Wire*.
 * An *Empty* cell is always left unchanged.

### Wireworld file format

The Wireworld file format is a simple plain text format. It is taken over from **Mark Owen**, who presents on his website a [Wireworld computer](http://www.quinapalus.com/wi-index.html) designed by him, **David Moore** and others, which is capable of computing prime numbers. We acknowledge that the example input file `primes.wi` is originating from [Mark Owen's website](http://www.quinapalus.com).

The first line of a Wireworld file contains 2 positive integers, specifying the width *W* (i.e. the number of cells in x-direction) and height *H* (i.e. the number of cells in y-direction) of the automaton.

The header is followed by exactly *H* lines containing exactly *W* characters (excluding the terminating line break character `'\n'`), representing the state of the cellular automaton. The individual cell states are encoded as follows:

| Character   | State           |
|-------------|-----------------|
| `'@'`       | *Electron head* |
| `'~'`       | *Electron tail* |
| `'#'`       | *Wire*          |
| (any other) | *Empty*         |

As an example, a Wireworld file (`diodes.wi`) containing 10x7 cells is given below (*Empty* is here encoded as `'.'`):

```
10 7
....##....
~@##.#####
....##....
..........
....##....
#### ###@~
....##....
```

#### Implementation detail

The number of cells in the effectively simulated automaton is *H* times (*W* +1), i.e. the terminating line feed character is interperted as extra *Empty* cell. It is important that the input file follows the **Unix convention for line endings**.

## Release Date

2016-10-24

## Version History

 * 2016-10-24: Initial Release on PRACE CodeVault repository

## Contributors

 * Thomas Steinreiter - [thomas.steinreiter@risc-software.at](mailto:thomas.steinreiter@risc-software.at) (C++14 version)
 * Thomas Ponweiser - [thomas.ponweiser@risc-software.at](mailto:thomas.ponweiser@risc-software.at) (C version)

## Copyright

This code is available under Apache License, Version 2.0 - see also the license file in the CodeVault root directory.


## Languages

Two versions of this sample are available, one written in C and another in C++ 14.


## Parallelisation

This sample uses MPI-3 for parallelization.

## Level of the code sample complexity

Intermediate / Advanced

## Compiling and Running

Follow the instructions given in the appropriate `README.md` in the `wireworld_c` or `wireworld_c++` sub-directory.
